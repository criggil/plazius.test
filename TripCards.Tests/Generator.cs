﻿using System.Collections.Generic;

namespace TripCards.Tests
{
    public class Generator
    {
        private readonly List<string> _tripCards = new List<string>()
        {
            "Амстердам",
            "Андорра-ла-Велья",
            "Афины",
            "Белград",
            "Берлин",
            "Берн",
            "Братислава",
            "Брюссель",
            "Будапешт",
            "Бухарест",
            "Вадуц",
            "Валлетта",
            "Варшава",
            "Ватикан",
            "Вена",
            "Вильнюс",
            "Дублин",
            "Загреб",
            "Киев",
            "Кишинёв",
            "Копенгаген",
            "Лиссабон",
            "Лондон",
            "Любляна",
            "Люксембург",
            "Мадрид",
            "Минск",
            "Монако",
            "Москва",
            "Осло",
            "Париж",
            "Подгорица",
            "Прага",
            "Рейкьявик",
            "Рига",
            "Рим",
            "Сан-Марино",
            "Сараево",
            "Скопье",
            "София",
            "Стокгольм",
            "Таллин",
            "Тирана",
            "Хельсинки",
            "Донецк",
            "Луганск",
            "Приштина",
            "Тирасполь"
        };

        public static List<TripCard> GetUnsortedList()
        {
            return new List<TripCard>()
            {
                new TripCard("Ватикан", "Вена"),
                new TripCard("Загреб", "Киев"),
                new TripCard("Вена", "Вильнюс"),
                new TripCard("Варшава", "Ватикан"),
                new TripCard("Вильнюс", "Дублин"),
                new TripCard("Дублин", "Загреб")
            };
        }

        public static List<TripCard> GetSortedList()
        {
            return new List<TripCard>()
            {
                new TripCard("Варшава", "Ватикан"),
                new TripCard("Ватикан", "Вена"),
                new TripCard("Вена", "Вильнюс"),
                new TripCard("Вильнюс", "Дублин"),
                new TripCard("Дублин", "Загреб"),
                new TripCard("Загреб", "Киев")
            };
        }
    }
}