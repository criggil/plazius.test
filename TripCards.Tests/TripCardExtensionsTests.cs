﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace TripCards.Tests
{
    [TestFixture]
    public class TripCardExtensionsTests
    {
        [Test]
        public void SortWithoutCaching_Null_ThrowException()
        {
            IList<TripCard> tripCards = null;

            Assert.Throws<ArgumentNullException>(() => tripCards.SortWithoutCaching());
        }

        [Test]
        public void SortWithoutCaching_EmptyList_Success()
        {

            var tripCards = new List<TripCard>();

            tripCards.SortWithoutCaching();

            Assert.NotNull(tripCards);
            Assert.Zero(tripCards.Count);
        }

        [Test]
        public void SortWithoutCaching_SortedList_Success()
        {
            var originCards = Generator.GetSortedList();
            var tripCards = Generator.GetSortedList();

            tripCards.SortWithoutCaching();

            Assert.NotNull(tripCards);
            Assert.NotZero(tripCards.Count);
            Assert.AreEqual(originCards.Count, tripCards.Count);
            for (var i = 0; i <= originCards.Count - 1; ++i)
            {
                Assert.AreEqual(originCards[i], tripCards[i]);
            }
        }

        [Test]
        public void SortWithoutCaching_RandomList_Success()
        {
            var originCards = Generator.GetSortedList();
            var tripCards = Generator.GetUnsortedList();

            tripCards.SortWithoutCaching();

            Assert.NotNull(tripCards);
            Assert.NotZero(tripCards.Count);
            Assert.AreEqual(originCards.Count, tripCards.Count);
            for (var i = 0; i <= originCards.Count - 1; ++i)
            {
                Assert.AreEqual(originCards[i], tripCards[i]);
            }
        }

        [Test]
        public void SortWithCaching_Null_ThrowException()
        {
            IList<TripCard> tripCards = null;

            Assert.Throws<ArgumentNullException>(() => tripCards.SortWithCaching());
        }

        [Test]
        public void SortWithCaching_EmptyList_Success()
        {
            IList<TripCard> tripCards = new List<TripCard>();

            tripCards.SortWithCaching();

            Assert.NotNull(tripCards);
            Assert.Zero(tripCards.Count);
        }

        [Test]
        public void SortWithCaching_SortedList_Success()
        {
            var originCards = Generator.GetSortedList();
            var tripCards = Generator.GetSortedList();

            tripCards.SortWithCaching();

            Assert.NotNull(tripCards);
            Assert.NotZero(tripCards.Count);
            Assert.AreEqual(originCards.Count, tripCards.Count);
            for (var i = 0; i <= originCards.Count - 1; ++i)
            {
                Assert.AreEqual(originCards[i], tripCards[i]);
            }
        }

        [Test]
        public void SortWithCaching_RandomList_Success()
        {
            var originCards = Generator.GetSortedList();
            var tripCards = Generator.GetUnsortedList();

            tripCards.SortWithCaching();

            Assert.NotNull(tripCards);
            Assert.NotZero(tripCards.Count);
            Assert.AreEqual(originCards.Count, tripCards.Count);
            for (var i = 0; i <= originCards.Count - 1; ++i)
            {
                Assert.AreEqual(originCards[i], tripCards[i]);
            }
        }
    }
}