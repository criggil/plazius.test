﻿using System;
using System.Collections.Generic;

namespace TripCards
{
    public static class TripCardExtension
    {
        /// <summary>
        /// Сортировка карточек.
        /// Сложность алгоритма O(n^2) по времени выполнения и O(1) по дополнительной памяти.
        /// </summary>
        /// <param name="tripCards">Список не отсортированных городов.</param>
        /// <returns>Список отсортированных городов.</returns>
        public static IList<TripCard> SortWithoutCaching(this IList<TripCard> tripCards)
        {
            tripCards.NullCheck();
            if (tripCards.Count == 0)
            {
                return tripCards;
            }
            
            // Поиск начала путишествия.
            for (var i = 0; i <= tripCards.Count - 1; ++i)
            {
                var isFound = false;
                for (var j = 0; j <= tripCards.Count - 1; ++j)
                {
                    if (tripCards[i].From == tripCards[j].To)
                    {
                        isFound = true;
                        break;
                    }
                }
                if (!isFound)
                {
                    tripCards.Swap(0, i);  // Установка начала путишествия в начало списка.
                    break;
                }
            }

            // Сортировка оставшихся карточек
            for (var i = 1; i < tripCards.Count - 1; ++i)
            {
                for (var j = i; j <= tripCards.Count - 1; ++j)
                {
                    if (tripCards[i-1].To == tripCards[j].From)
                    {
                        tripCards.Swap(i, j); // Перестановка карточек.
                        break;
                    }
                }
            }

            return tripCards;
        }

        /// <summary>
        /// Сортировка карточек с кэшированием городов назначений и индексов для быстрого поиска.
        /// Сложность алгоритма O(n) по времени выполнения и O(n) по дополнительной памяти для кэширования.
        /// </summary>
        /// <param name="tripCards">Список не отсортированных городов.</param>
        /// <returns>Список отсортированных городов.</returns>
        public static IList<TripCard> SortWithCaching(this IList<TripCard> tripCards)
        {
            tripCards.NullCheck();
            if (tripCards.Count == 0)
            {
                return tripCards;
            }

            
            var toCities = new HashSet<string>();  // Множество городов назначения для быстрого поиска начала путишествия.
            var fromIndexes = new Dictionary<string, int>(tripCards.Count); // Словарь городов отправления с индексами в списке для быстрой замены.
            for (var i = 0; i <= tripCards.Count - 1; ++i)
            {
                fromIndexes[tripCards[i].From] = i;
                toCities.Add(tripCards[i].To);
            }

            // Поиск начала путишествия.
            for (var i = 0; i < tripCards.Count; ++i)
            {
                if (!toCities.Contains(tripCards[i].From))
                {
                    fromIndexes[tripCards[0].From] = i;  // Обновление индекса.
                    tripCards.Swap(0, i);  // Установка начала путишествия в начало списка.
                    break;
                }
            }

            // Сортировка оставшихся карточек
            for (var i = 1; i < tripCards.Count - 1; ++i)
            {
                var toCity = tripCards[i - 1].To;
                var cardIndex = fromIndexes[toCity];
                
                fromIndexes[tripCards[i].From] = cardIndex; // Обновление индекса.
                fromIndexes.Remove(toCity);
                tripCards.Swap(i, cardIndex); // Перестановка карточек.
            }

            return tripCards;
        }

        private static void NullCheck(this IList<TripCard> tripCards)
        {
            if (tripCards == null)
            {
                throw new ArgumentNullException();
            }
        }

        private static IList<TripCard> Swap(this IList<TripCard> tripCards, int i, int j)
        {
            if (i != j)
            {
                var card = tripCards[i];
                tripCards[i] = tripCards[j];
                tripCards[j] = card;
            }
            return tripCards;
        }
    }
}