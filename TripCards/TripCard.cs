﻿namespace TripCards
{
    /// <summary>
    /// Карточка путишествия.
    /// </summary>
    public class TripCard
    {
        public TripCard(string from, string to)
        {
            From = from;
            To = to;
        }

        /// <summary>
        /// Город начала путишествия.
        /// </summary>
        public string From { get; private set; }
        
        /// <summary>
        /// город окончания путишествия.
        /// </summary>
        public string To { get; private set; }

        public override bool Equals(object obj)
        {
            var card = obj as TripCard;
            if (card != null)
            {
                return (From == card.From) && (To == card.To);
            }
            return false;
        }
    }
}